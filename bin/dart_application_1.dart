import 'dart:io';
import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

void main() {
  var winList = {
    '1stPrice': '436549',
    '2stPrice': '436595',
    '3stPrice': '436593',
    '4stPrice': '502412',
    '5stPrice': '049364',
    '6stPrice': '396501',
    '7stPrice': '285563',
    '8stPrice': '511577',
    '9stPrice': '051550',
    '10stPrice': '223796'
  };
  print('Please input num ');
  var num = stdin.readLineSync()!;

  if (isWin(num, winList)) {
    print('$num is win in ${getPrice(num, winList)}');
  } else {
    print('$num is not win');
  }
}

bool isWin(num, winList) {
  for (var value in winList.values) {
    if (num == value) {
      return true;
    }
  }
  return false;
}

String getPrice(num, winList) {
  var price = winList.keys.firstWhere((k) => winList[k] == '$num');
  return price;
}
